---
marp: true
paginate: true
theme: default
---
<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->
![bg left:40% 80%](img/ansible.png)



## Ansible 101 

### Concepts et principes de base

-emmanuel.braux@imt-atlantique.fr-

---

# Licence informations


Auteur : emmanuel.braux@imt-atlantique.fr

Cette présentation est sous License Créative Commons 3.0 France (CC BY-NC-SA 3.0 FR)
Selon les options : Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions.

![cc-by-nc-sa](img/cc-by-nc-sa.png)

---
<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->

![bg left:30% 80%](img/ansible.png)

# Ansible 101

- **Automatisation et Infrastructure as code**
- **Ansible**
- **Installation et Configuration**
- **Modules, Plugins et Mode Ad-Hoc**
- **Les Playbooks**
- **Les Variables et les Templates**
- **Les Structures de contrôle**
- **Les Rôles**
- **Les Collections**
  
---
<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->

![bg left:30% 80%](img/ansible.png)


## > Automatisation et Infrastructure as code
- **Ansible**
- **Installation et Configuration**
- **Modules, Plugins et Mode Ad-Hoc**
- **Les Playbooks**
- **Les Variables et les Templates**
- **Les Structures de contrôle**
- **Les Rôles**
- **Les Collections**
  
---
# Le quotidien des OPS : Tâches IT 

- Gestion de serveurs
- Gestion de configuration
- Déploiement d'applications et de services
- Traitement de données
- ...

---
# L'Automatisation

- Réaliser des tâches sans intervention humaine
- Gagner du temps, 
- Gagner de la fiabilité, de la sécurité
- ...


---
# Automatisation - Approches

Deux approches : 

- Procédurale : 
  - Description précise des commandes/actions de chaque étape
  - Puis automatisation via des scripts
- Déclarative :
  - On écrit ce qui doit être,
  - Un outil de gestion va assurer que l'état réel  de l'infrastructure est celui défini dans le code
  - Si l'état voulu est déjà en place l'outil de gestion ne ré-exécute pas la tâche. 

---

# L'orchestration

**Mise en place de Worflows complets de déploiement, configuration,  suivi, ...**

- Configuration d'OS :  Linux, windows, ...
- Configuration d'applications : Apache, Mysql, Hadoop, ...
- Déploiement de serveurs : machines physiques (ipmi), machine virtuelle, Cloud, container (docker, LXC, ...)
- Stockage, Réseau, Gestion d'identité, ...
- Interaction avec outils comme Jira, Git, Github, Gitlab ,...
- ...

---
# Reproductibilité

- **Reconstruire** une infrastructure à l’identique :
  -  Indicent majeur
  -  Changement de fournisseur
  -  ...
- **Dupliquer** des environnements :
  - Production, Intégration, Recette, Développement, ...


---
# Infrastructures modernes 

Microservices, Devops, ...

- Le nombre de serveurs nécessaires pour une infrastructure a "explosé"
- Complexité du déploiement des ressources, leur interactions, ...
- Interaction avec des outils en mode Saas
- Déploiement dynamique
  - Adaptation à la demande
  - Gestion du coût

## La gestion manuelle est devenue impossible


---
# Infrastructure as Code : IaC

 **Définir une infrastructure comme le code d'un logiciel.**

- Utilisation des mêmes méthodes : 
  - Code source
  - Collaboration
  - Gestion de version
  - Tests 
  - Intégration continue 
  - Déploiement continu

---
 
# Infrastructure as code : Pets vs Cattle

![h:400](img/pets_cattle.jpg)


---
<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->

![bg left:30% 80%](img/ansible.png)

- **Automatisation et Infrastructure as code**
## > Ansible
- **Installation et Configuration**
- **Modules, Plugins et Mode Ad-Hoc**
- **Les Playbooks**
- **Les Variables et les Templates**
- **Les Structures de contrôle**
- **Les Rôles**
- **Les Collections**

---
# Ansible

![w:200 center](img/ansible.png) est un outil d'**Automatisation de tâches IT**, et d'**Orchestration** d'infrastructure

- Logiciel libre, disponible sous licence GPL v3
- Créé en 2012 par Michael De Haan, édité depuis 2015 par RedHat (Groupe IBM)
- Développé en Python.

**Conçu pour être simple !!**

---

# Atouts d'Ansible

## Simple a installer

- Pas d'agent sur les serveurs à gérer
- Uniquement un accès SSH

## Simple à prendre en main

- Utilisation de fichier texte, lisibles, pas de code à développer
- Courbe d'apprentissage basse,
- Transcription très simples de commandes shell

---

## Simple à comprendre, et prévisible

- Les tâches s'exécutent les unes après les autres

## Robuste

- Pas de "master"

## Puissant

- Évolution progressive vers les fonctionnalités avancées
- Beaucoup d'exemples, de documentation
- Beaucoup de ressources directement utilisables
  
---

# L'idempotence

***Une opération a le même effet qu'on l'applique une ou plusieurs fois.***

Ansible garantit l'idempotence de l'ensemble de ses modules.

Une tâche peut être lancée plusieurs fois :
- l’état final reste le même que lors du premier lancement.
- seules les actions nécessaires seront exécutées.

Par exemple, le module de création de dossier ne créera le dossier que s'il n'existe pas déjà.

---

# Inconvénients 


- Parfois moins bien que ces concurrents (Puppet, salt, Chef,Terraform, ...), mais couverture fonctionnelle plus complète
- Principe d'indépendance des tâches pas toujours simple à gérer:  suivi de variables, ...
- Nécessite de respecter l'idempotence dans la définition de ses tâches.
- Problème de performance si beaucoup d'hôtes à gérer : Mode push = goulet d'étranglement  (un mode Pull existe mais très peu utilisé)

---

# Intérêt pour Ansible - Google Trends  


![h:500](img/gtrends-ansible.png)

**Surtout pour des utilisations dans les domaines du Devops et du Cloud**

---
# Architecture

![height:500px](img/ansible-archi_simple.jpg)

---
# Vocabulaire

Listes de serveurs à gérer : **inventory**

Execution de **tâches**

Une exécution simple de tâches :

- soit en ligne de commande : **mode AdHoc**
- soit dans un fichier YAML permettant de regrouper plusieurs tâches : **les playbooks**

Utilisation de tâches prêtes à l'emploi : **les modules**

---
# Pour aller un peu plus loin

Ansible sait s'appuyer sur des **templates** : jinja2
- manipulation de variables
- mise en forme
- conditions

Les playbooks proposent de nombreuses fonctionnalités avancées :

- gestion de **conditions**,
- ...

---
# Et encore un peu plus loin 

Protéger des données, mots de passe : **Ansible Vault**

Limiter l'exécution à certaines tâches en fonction du contexte : **les Tags**

...

---
# Organiser ses déploiements

Regrouper des tâches et fichiers de configuration dans **des Rôles** pour : 

- Approche fonctionnelle
- Rapidité de configuration
- Rapidité de mise en oeuvre, robustesse
- Partage
- ...

> Orientation fonctionnelle : ex rôle "serveur NFS"

---
# Étendre les fonctionnalités d'Ansible  

Ajouter des fonctionnalités à Ansible en utilisant **des collections** 
- des modules
- des filtres de traitement de données
- ...
  
> Les collections sont également très utilisées pour regrouper des rôles.


---
# Connexion SSH

Ansible s'appuie sur SSH. 

- Authentification par Clé SSH ou par mot de passe
- Possibilité de demander un mot de passe pour les opérations nécessitant des droits root.
- Possibilité d'élévation des privilèges de l'utilisateur utilisé lors de la connexion : **become**
- ...

**Si pas d'accès SSH, c'est qu'on a pas le droit de gérer la machine ...**

**Idéalement mettre en place une connexion ssh, avec clé SSH , et un utilisateur non root, avec des droits "sudo"**

---

# Pour résumer, pour utiliser Ansible 

1. Installer Ansible sur une machine de management le "Controller-Node"
   - Machine serveur ou poste de travail
   - Pas de base de données, pas d'installation sur les postes clients, ...
   - Nécessite Python
2. Configurer hôtes avec lesquels on doit travailler : les "Managed nodes"
   - Les déclarer dans l'inventory
   - Configurer l'accès SSH depuis la machine de management
   - Nécessite Python (installable avec Ansible)
3. Lancer des tâches, ou écrire (ou récupérer) des fichiers de taches.

---
<!-- _class: invert -->
<!-- _color: white -->

# LAB "decouverte"  

[https://ebraux.gitlab.io/ansible-labs/](https://ebraux.gitlab.io/ansible-labs/)

🗹 Déployer l'environnement de lab, et le tester
🗹 se connecter au **Control-Node**
🗹 Tester le modèle Pet vs Cattle


---
<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->

![bg left:30% 80%](img/ansible.png)

- **Automatisation et Infrastructure as code**
- **Ansible**
## > Installation et Configuration
- **Modules, Plugins et Mode Ad-Hoc**
- **Les Playbooks**
- **Les Variables et les Templates**
- **Les Structures de contrôle**
- **Les Rôles**
- **Les Collections**
---
# Installation - Packages

## Packages éditeurs

Ansible est intégré dans les packages officiels de la plupart des distributions : Red Hat, CentOS, Fedora, Debian, Ubuntu, ... 

- Souvent en retard de plusieurs versions
- Pas forcément l'ensemble des modules

## Packages "Ansible"

Ansible propose également ses propres dépôts, avec des versions à jour.

---
# Installation autres 

## Python pip

Ansible est écrit en python, et est disponible sous forme d'un module installable avec "pip" (Python package manager)

## Code Source

Il est également possible de l'installer depuis le code source : [https://github.com/ansible/ansible](https://github.com/ansible/ansible)

---

# Configuration du comportement d'Ansible

##  ansible.cfg

- Par défaut : /etc/ansible/ansible.cfg
- Alternative : dans le répertoire courant, dans le homedir, utilisation de variables d'environnement
- Configuration de la connexion SSH
- Chemins par défaut (playbooks, rôles, ...)
- ...

*[https://docs.ansible.com/ansible/latest/reference_appendices/config.html](https://docs.ansible.com/ansible/latest/reference_appendices/config.html)*

---
## Exemple de fichier ansible.cfg

``` bash
[defaults]
inventory = inventory/hosts


host_key_checking = False

roles_path              = roles:../roles
template_path           = template
log_path                = logs/ansible.log
remote_user             = automation-user
ansible_managed         = FILE MANAGED VIA ANSIBLE, DIRECT EDITS NOT PERSISTED 

#forks                   = 10

[ssh_connection]
ssh_args = -o ForwardAgent=yes  -o ControlPersist=60s 
```

---

# Liste des machines cible
 
## Inventory 

- Par défaut : /etc/ansible/hosts
- Alternative : définition dans `ansible.cfg`, option de la ligne de commande
- Utilisation Basique : 
    - Fichier text, en YAML ou Json
    - Structuration en groupes de machines (Production/Test, Frontend/Backend, ...)

*[https://docs.ansible.com/ansible/latest/inventory_guide/intro_inventory.html](https://docs.ansible.com/ansible/latest/inventory_guide/intro_inventory.html)*

--- 
## Exemple de fichier inventory

```ini
[prod:children]
frontend-prod
backend-prod

[frontend-prod]
srv-front-01
srv-front-02

[backend-prod]
srv-back-01

[rec]
srv-rec-01

```

---
## Au format Yaml
``` yaml
all:
  children:
    prod:
      children:
        frontend-prod:
        backend-prod:

    frontend-prod:
      hosts:
        srv-front-01:
        srv-front-02:

    backend-prod:
      hosts:
        srv-back-01:

    rec:
      hosts:
        srv-rec-01:

```

---
#  Utilisation avancée

**Inventaire dynamique** : des plugins d’inventaire pour obtenir la liste des `hosts` depuis des solutions externes :
- Des fournisseurs de ressources : AWS, Azure, GCP, ...
- Des outils de virtualisation : Vsphere, Libvirt, Vagrant, ...
- Des systèmes de monitoring : Zabbix, ...
- ...

*[https://docs.ansible.com/ansible/latest/inventory_guide/intro_dynamic_inventory.html](https://docs.ansible.com/ansible/latest/inventory_guide/intro_dynamic_inventory.html)*
*[https://docs.ansible.com/ansible/latest/collections/index_inventory.html](https://docs.ansible.com/ansible/latest/collections/index_inventory.html)*

**Projet de CMDB** :  *[https://ansible-cmdb.readthedocs.io/en/latest/](https://ansible-cmdb.readthedocs.io/en/latest/)*



---
<!-- _class: invert -->
<!-- _color: white -->

# LAB "inventory"  

🗹 Créer un "inventory" simple, et observer comment Ansible l'interprète
🗹 Créer des "inventory" plus complexes

---
<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->

![bg left:30% 80%](img/ansible.png)

- **Automatisation et Infrastructure as code**
- **Ansible**
- **Installation et Configuration**
## > Modules, Plugins, et Mode Ad-Hoc
- **Les Playbooks**
- **Les Variables et les Templates**
- **Les Structures de contrôle**
- **Les Rôles**
- **Les Collections**
- 
---

# Les Modules

Les tâches exécutées avec Ansible s'appuient sur des **modules**.

- Un module :
  - Apporte des fonctionnalités, des actions qui pourront être lancées sur les Managed-Nodes.
  - C'est une partie de code, généralement en python, ajoutée à Ansible
  - Permet d'interagir avec les ressources du système, d’exécuter des commandes, ...
- De nombreux modules sont disponibles.
- Il est possible d'écrire ses propres modules.


---
# Les modules les plus communément utilisés

- ansible.builtin.apt / ansible.builtin.yum : gérer des package (installer, supprimer, ...)
- ansible.builtin.file : gérer des fichiers et dossiers
- ansible.posix.authorized_key module : gérer les "SSH authorized key"
- ansible.builtin.setup : collecter des informations sur les Managed-Nodes
- ...

*[https://docs.ansible.com/ansible/latest/collections/index_module.html](https://docs.ansible.com/ansible/latest/collections/index_module.html)*

---
# Les plugins

- Comme les modules, c'est une partie de code ajoutée à Ansible
- Ajoute des fonctionnalités à l'exécution d'Ansible 
  - Filtres de traitement de données
  - Gestion de variables
  - Fonctions de test
  - ...
- De nombreux plugins sont disponibles.
- Il est possible d'écrire ses propres plugins.

*[https://docs.ansible.com/ansible/latest/module_plugin_guide/index.html](https://docs.ansible.com/ansible/latest/module_plugin_guide/index.html)*

---

# Le Mode Ad-HOC

## Utilisation d'Ansible en ligne de commande.
`ansible all -m ansible.builtin.shell -a 'free -h'`

Usage basique, pour faire des opérations exceptionnelles :

- Arrêter/rebooter certains serveurs
- Récupérer les informations sur un systéme
- ...
 
> Toutes les "Ad-Hoc command" peuvent être utilisée dans un playbook. Et dans la plupart des cas, autant faire un playbook basique.


---
<!-- _class: invert -->
<!-- _color: white -->

# LAB "adhoc" : Configuration d'Ansible et utilisation de commandes Ad-Hoc

🗹 Réaliser une configuration minimum d'Ansible
🗹 Tester l'accès aux **Managed-Nodes**

---
<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->

![bg left:30% 80%](img/ansible.png)

- **Automatisation et Infrastructure as code**
- **Ansible**
- **Installation et Configuration**
- **Modules, Plugins et Mode Ad-Hoc**
## > Les Playbooks
- **Les Variables et les Templates**
- **Les Structures de contrôle**
- **Les Rôles**
- **Les Collections**

---
# Les playbooks

## Si vous devez exécuter des tâches plus d'une fois, écrivez un playbook !!

Descriptions des tâches dans un fichier.

2 sections :

- **targets section** : les Managed-node qui vont être traités
- **tasks section**   : liste de  tâches à exécuter (dans l'ordre)

Fichier au format YAML = c'est du texte, donc facilement versionnable
 
---

# Quelques notions de YAML

- Les fichiers démarrent avec `---`
- L'indentation est importante
- Variables, Dictionnaire (clé/valeur), et listes :
```yaml
  - martin:
      name: Martin SMITH
      job: Developer
      skills:
        - python
        - node.js
        - java
```
---

# Exemple de playbook

Installer le package "fail2ban" sur tous les "Managed-nodes" : 
```yaml
- hosts: all

  tasks:
  - ansible.builtin.apt:
      name: fail2ban
      state: latest
```

---
# Nommer les choses

```yaml
- name: Playbook de demo apt
  hosts: all

  vars:
    package_name: fail2ban

  tasks:
  - name: Installer un package avec le module apt
    ansible.builtin.apt:
      name: fail2ban
      state: present
```

---

# Quelques options

- Adapter/configurer l'exécution par Ansible
```yaml
- name: update web servers
  hosts: webservers
  remote_user: ubuntu
  become: yes
```
- Inclure de multiples playbooks dans un playbook principal
```yaml
  - ansible.builtin.import_playbook: config_users.yml
  - ansible.builtin.import_playbook: fail2ban.yml
```
- ...
  
---

<!-- _class: invert -->
<!-- _color: white -->

# LAB "playbook" : Utiliser des playbooks

🗹 Lancer des playbooks
🗹 Tester des fonctionnalités

---

<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->

![bg left:30% 80%](img/ansible.png)

- **Automatisation et Infrastructure as code**
- **Ansible**
- **Installation et Configuration**
- **Modules, Plugins et Mode Ad-Hoc**
- **Les Playbooks**
## > Les Variables et les Templates
- **Les Structures de contrôle**
- **Les Rôles**
- **Les Collections**


---
# Les types de variables

- Variables simples
- Listes
- Dictionnaires
  
*Rem : il est possible d'appliquer des filtres JinJa2 (upper, lower, ...)*

---

# Définition des variables 

Les variables peuvent être définies :

- Dans un playbook 
- Dans un rôle
- Dans l'inventory : 
  - directement dans le fichier principal 
  - dans une arborescence structurée en "group_vars" et "host_vars"
- Via la ligne de commande
- Pendant l'exécution d'une tâche

**L'ordre de priorité est important !!**

---
# Les Ansible Facts

Ansible collecte des informations sur les Managed-Nodes

- Système d'exploitation
- Configuration réseau
- Configuration matérielle
- ...

---
## Exemple d'utilisation de variables 

```yaml
- name: Playbook de demo apt
  hosts: all

  vars:
    package_name: fail2ban

  tasks:
  - name: Installer un package avec le module apt
    ansible.builtin.apt:
      name: "{{ package_name }}"
      state: present
```

---

<!-- _class: invert -->
<!-- _color: white -->

# LAB "variable" : Les variables

🗹 Créer différent type de variables
🗹 Tester l'ordre des priorités
🗹 Utiliser des Facts

---
# Les Templates : utilisation de Jinja2

- Remplacement de variables
- Instructions avancées : "for", "if/then/else", ...
- Commentaires
- Des filtres de transformation de contenu
- ...

---

# Exemple de template

Fichier de template : 

```jinja2
{% for user in users %}
   login={{ user.login }}, email={{ user.email }}
{% endfor %}
```

Utilisation : 
```yaml
- name: Template a file to /etc/userlist.conf
  ansible.builtin.template:
    src: templates/userlist.j2
    dest: /etc/userlist.conf
```

---
<!-- _class: invert -->
<!-- _color: white -->

# LAB "template" : Les templates

🗹 Générer un fichier de configuration

---
<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->

![bg left:30% 80%](img/ansible.png)

- **Automatisation et Infrastructure as code**
- **Ansible**
- **Installation et Configuration**
- **Modules, Plugins et Mode Ad-Hoc**
- **Les Playbooks**
- **Les Variables et les Templates**
## > Les Structures de contrôle
- **Les Rôles**
- **Les Collections**

---

# Les boucles

Permet d'exécuter une tâche plusieurs fois.
Par exemple pour installer plusieurs packages :

```yaml
tasks:
  - name: install several package
    ansible.builtin.apt:
      name: "{{ item }}"
      state: latest
    loop:
       - fail2ban
       - rsync
```
*Anciennement `with_items`*

---

# Les séquences 

- Génération d'une séquence de nombres.
- Définition : début (start), fin (end), intervalle (stride)
- Possibilité de définir un format pour créer une chaîne

Par exemple pour créer des utilisateurs : 

```yaml
tasks:
- name: create some test users
  ansible.builtin.user:
    name: "{{ item }}"
    state: present
  with_sequence: start=1 end=10 format=student%02x
```

*[https://docs.ansible.com/ansible/latest/collections/ansible/builtin/sequence_lookup.html](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/sequence_lookup.html)*

---
# Séquences - exemples

- Créer 4 éléments :
  `with_sequence: count=4`
- Créer des nombres paires de 4 a 16 :
  `with_sequence: start=4 end=16 stride=2`
- Créer un compte à rebours :
  `with_sequence: start=10 end=0 stride=-1`

---
# Les conditions

Pour n'exécuter une tâche que si nécessaire

Example
```yaml
tasks:
  - name: Debian/Ubuntu | Install Apache server
    ansible.builtin.apt:
      name: "apache2"
      state: latest
    when: ansible_os_family == "Debian"    
```

---
# Blocks 

Regrouper des tâches

```yaml
tasks:
 - name: Handle the error
   block:
     - debug:
         msg: 'I execute normally'
     - name: i force a failure
       command: /bin/false
     - debug:
         msg: 'I never execute, due to the above task failing, :-('
   rescue:
     - debug:
         msg: 'I caught an error, can do stuff here to fix it, :-)'
```

---
# Blocks - Optpimiser les condiditions

Exemple : regroupper des taches avec une condition

``` yaml
   - name: Debian/Ubuntu | Install Mariadb dependencies on Debian/Ubuntu systems
      block:
        
        # Ensemble de taches     
        ...
        - name: Debian/Ubuntu | Install Mariadb and dependencies on Debian/Ubuntu systems
        ...

      when: ansible_os_family == "Debian"    
```

---
# Blocks - Gestion des erreurs (try/catch)

Example améliorer la gestion des erreurs (try/catch) :
- Utilisation des mots clé "rescue" et "always"
  
```yaml
tasks:
 - name: Handle the error
   block:
     - debug:
         msg: 'I execute normally'
     - name: i force a failure
       command: /bin/false
     - debug:
         msg: 'I never execute, due to the above task failing, :-('
   rescue:
     - debug:
         msg: 'I caught an error, can do stuff here to fix it, :-)'
```

---
<!-- _class: invert -->
<!-- _color: white -->

# LAB "control" : Les structures de contrôle

🗹 Tester les différentes structures de contrôle

---

<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->

![bg left:30% 80%](img/ansible.png)

- **Automatisation et Infrastructure as code**
- **Ansible**
- **Installation et Configuration**
- **Modules, Plugins et Mode Ad-Hoc**
- **Les Playbooks**
- **Les Variables et les Templates**
- **Les Structures de contrôle**
## > Les Rôles
- **Les Collections**

---

# Ansible Rôles

Objectif : rassembler dans un package tâches, variables, playbooks, fichiers, templates, ...

Orienté fonctionnel : gestion d'un logiciel (installer, configurer, ... )

Intérêt :
- Capitalisation : ré-utiliser le rôle dans plusieurs playbooks.
- Partage

Le fonctionnement du rôle doit pouvoir être adapté avec des variables.

---

# Structure d'un rôle

Un rôle est structuré en dossier: 

- **tasks** : les tâches,
- **handler** : les actions sur notification
- **files** : fichiers
- **template** :  les templates au format Jinja2
- **vars** : variables
- **defaults** : valeurs par défaut 

Chaque dossier contient un fichier 'main.yml', utilisé par défaut (sauf pour templates)

*[https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_reuse_roles.html](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_reuse_roles.html)*

---

# Utilisation d'un rôle

```yaml
---
- name: Sécurisation des managed- node
  hosts: all
  roles:
    - hardening
```

---
# Partage d'un rôle : Ansible-galaxy

- Portail qui permet de publier et diffuser des éléments liè à Ansible, dont les rôles
- Mis à la disposition de la communauté par ansible
- Intégré à ansible (commande `ansible-galaxy`)
- Indexation par tags, score qualité, popularité

Ansible galaxy [https://galaxy.ansible.com/](https://galaxy.ansible.com/)


*Les rôles publiés peuvent être utilisés directement ou servir d'inspiration pour réaliser vos premiers rôles.*

---
# Gérér les Rôles

- Installation :
  - Avec la commande `ansible-galaxy`
  - Avec un fichier de `requirements`
- Source
  - Ansible Galaxy
  - Un fichier local, ou accessible en http : une archive (tgz, ...)
  - Un dépot git

  
*[https://galaxy.ansible.com/docs/contributing/creating_role.html](https://galaxy.ansible.com/docs/contributing/creating_role.html)*
*[https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#installing-roles-from-galaxy](https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#installing-roles-from-galaxy)*

---
<!-- _class: invert -->
<!-- _color: white -->

# LAB "role" : Utiliser des rôles

🗹 Utiliser et observer un rôle basique
🗹 Utiliser et observer un rôle d'Ansible Galaxy

---

<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->

![bg left:30% 80%](img/ansible.png)

- **Automatisation et Infrastructure as code**
- **Ansible**
- **Installation et Configuration**
- **Modules, Plugins et Mode Ad-Hoc**
- **Les Playbooks**
- **Les Variables et les Templates**
- **Les Structures de contrôle**
- **Les Rôles**
## > Les Collections

---
# Historique

Jusqu'à la version 2.9 :
- Ansible = un gros dépôt centralisé
- Contient le coeur d'Ansible
- Mais également toutes les contributions de la communauté
- Plus de 4000 modules, plus des 3/4 supportés par la communauté
- Plus de 4 000 issues, et 2000 pull request.

Une évolution était necessaire pour "libérer" les développements.

---
# Les collections 

- Le projet Ansible ne contient plus que les briques "core"
  - Binaires Ansible
  - Principaux modules
- Les autres modules sont gérés de façon indépendante, instalable à la demande
- Modèle de packaging : les collections
  - Cycle de vie indépendant, correctifs déployés fréquement
  - Intégré à Galaxy
  - Contient des modules, mais également des playbooks, roles, et plugins.


---
# Fully Qualified Collection Name

- Format de nom des collections - 2 parties : 
  - Le nom de l'auteur qui maintient la Collection (namespace)
  - Le nom de la Collection
- Ex : 
  - ansible.builtin
  - community.general
  - amazon.aws
  - my_namespace.my_collection
  -  ...
  
---
# Impact sur la syntaxe 

Noms des modules :


| version  2.9 | version 2.10  |
| ----- | ------- |
| apt | ansible.builtin.apt |
| file | ansible.builtin.file |
| authorized_key | ansible.posix.authorized_key |

> Pour l'instant les 2 formats sont acceptés en version 2.9 et supérieure


---
# Collection "ansible.builtin"
 
Ansible-core comprend uniquement la collection "ansible.builtin".

- Un peu moins de 100 modules
- Les plugins pricipaux
- Un peu plus de 600 issues, et 321 pull request.

*[https://docs.ansible.com/ansible/latest/collections/ansible/builtin/index.html](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/index.html)*

---
# Les principales collections

- Plus de 100 collections "officielles" (hébergée et documentées sur ansible.com) : Ansible, Openstack, Azure, OpenVSwitch, Netapp, ...
- Supportées par les éditeurs, ou la communauté (community.xxx)
- La plus importante : "community.general"
  - Plus de 500 modules
  - plugins, filters, ...

*[https://docs.ansible.com/ansible/latest/collections/index.html](https://docs.ansible.com/ansible/latest/collections/index.html)*
*[https://docs.ansible.com/ansible/latest/collections/community/general/index.html#plugins-in-community-general](https://docs.ansible.com/ansible/latest/collections/community/general/index.html#plugins-in-community-general)*

---
# Gérér les Collections

- Installation :
  - Avec la commande `ansible-galaxy`
  - Avec un fichier de `requirements`
- Source
  - Ansible Galaxy
  - Un fichier local, ou accessible en http : une archive (tgz, ...)
  - Un dossier local
  - Un dépot git
  
  
*[https://docs.ansible.com/ansible/latest/collections_guide/collections_installing.html](https://docs.ansible.com/ansible/latest/collections_guide/collections_installing.html)*
*[https://docs.ansible.com/ansible/latest/dev_guide/developing_collections_creating.html](https://docs.ansible.com/ansible/latest/dev_guide/developing_collections_creating.html)*

---


<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->

![bg left:30% 80%](img/ansible.png)

# Pour aller plus loin ...

---
# Quelques ressources

- [https://docs.ansible.com/](https://docs.ansible.com/)

- [https://ebraux.gitlab.io/ansible/](https://ebraux.gitlab.io/ansible/)
- Blog de Stéphane Robert : [https://blog.stephane-robert.info/tags/ansible/](https://blog.stephane-robert.info/tags/ansible/)

