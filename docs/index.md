---
hide:
  - navigation
  - toc
---

# Ansible - 101

Concepts de base et prise en main

---

Slides de la présentation:

- Version en ligne: [ansible-101.html](ansible-101.html)
- version PDF: [ansible-101.pdf](ansible-101.pdf)

---

Les labs correspondants à cette session sont disponibles :

- version en ligne : [https://ebraux.gitlab.io/ansible-labs/](https://ebraux.gitlab.io/ansible-labs/)
- dépôt Gitlab : [https://gitlab.com/ebraux/ansible-labs](https://gitlab.com/ebraux/ansible-labs)

---

![image alt <>](assets/ansible.png)



