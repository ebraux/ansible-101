# Utilisation en local du site

## Génération du PDF

```bash
cd slides
mkdir output
chmod 777 output
docker run -it --rm  --init -v ${PWD}:/home/marp/app/ -e LANG=$LANG marpteam/marp-cli ansible-101.md --allow-local-files --pdf -o output/ansible-101.pdf
```

Watch mode
```bash
docker run --rm --init -v $PWD:/home/marp/app/ -e LANG=$LANG -p 37717:37717 marpteam/marp-cli -w ansible-101.md
```

Server mode (Serve current directory in http://localhost:8080/)
```bash
docker run --rm --init -v $PWD:/home/marp/app -e LANG=$LANG -p 8080:8080 -p 37717:37717 marpteam/marp-cli -s .
```

---

## Génération du site web

Build de l'image
```bash
docker build -t mkdocs_ansible-101 .
```

Lancement en mode "serveur"
```bash
docker run  -v ${PWD}:/work -p 8000:8000 mkdocs_ansible-101 mkdocs serve -a 0.0.0.0:8000 --verbose
```

Genération du site
```bash
docker run  -v ${PWD}:/work -p 8000:8000 mkdocs_ansible-101 mkdocs build --strict --verbose
```

Ménage
```bash
docker run -v ${PWD}:/work mkdocs_ansible-101 rm -rf /work/site
docker image rm mkdocs_ansible-101
```

